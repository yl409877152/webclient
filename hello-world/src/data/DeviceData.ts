import Devices from './../model/Devices'
class DeviceData {
    public get tableData(): any {
        return this._tableData
    }

    public set tableData(value: any) {
        this._tableData = value
    }

    public get tableTitle(): any {
        return this._tableTitle
    }

    public set tableTitle(value: any) {
        this._tableTitle = value
    }

    public get deviceData(): any[] {
        return this._deviceData
    }

    public set deviceData(value: any[]) {
        this._deviceData = value
    }

    private _deviceData = [] // 二级设备和三级设备的数据
    private _tableData = {} //三级设备产生的数据
    private _tableTitle = {}

    constructor() {
        this.init()
    }

    public init() {
        this.tableTitle = {
            102: {
                tabTitleOne: [
                    {
                        temp: false,
                        prop: 'name',
                        label: '名称',
                        width: '180'
                    },
                    {
                        temp: false,
                        prop: 'address',
                        label: '位置',
                        width: '90'
                    },
                    {
                        temp: false,
                        prop: 'brand',
                        label: '品牌',
                        width: '90'
                    },
                    {
                        temp: true,
                        prop: 'system',
                        label: '输出压力',
                        width: '292'
                    },
                    {
                        temp: true,
                        prop: 'left',
                        label: '左侧',
                        width: '292'
                    },
                    {
                        temp: true,
                        prop: 'right',
                        label: '右侧',
                        width: '292'
                    }
                ],
                tabTitleTwo: [
                    {
                        type: 'left',
                        prop: 'left_pressure',
                        label: '压力',
                        width: '90'
                    },
                    {
                        type: 'left',
                        prop: 'left_workStatus',
                        label: '工作状态',
                        width: '90'
                    },

                    {
                        type: 'left',
                        prop: 'left_alarmStatus',
                        label: '报警状态',
                        width: '90'
                    },
                    {
                        type: 'right',
                        prop: 'right_pressure',
                        label: '压力',
                        width: '90'
                    },
                    {
                        type: 'right',
                        prop: 'right_workStatus',
                        label: '工作状态',
                        width: '90'
                    },

                    {
                        type: 'right',
                        prop: 'right_alarmStatus',
                        label: '报警状态',
                        width: '90'
                    },
                    {
                        type: 'system',
                        prop: 'system_pressure',
                        label: '压力',
                        width: '90'
                    },
                    {
                        type: 'system',
                        prop: 'system_alarmStatus',
                        label: '报警状态',
                        width: '90'
                    }
                ]
            }
        }
        this.tableData = {
            3: [
                {
                    id: '',
                    typeId: 102,
                    name: '医用氧气',
                    address: 'a',
                    brand: '雅森',
                    left_pressure: '1KPa',
                    left_workStatus: '2KPa',
                    left_alarmStatus: '3KPa',
                    right_pressure: '4KPa',
                    right_workStatus: '5KPa',
                    right_alarmStatus: '6KPa',
                    system_pressure: '4KPa',
                    system_alarmStatus: '6KPa'
                }
            ]
        }
       // this.initDeviceData()
    }
    public async initDeviceData(){
       const res = await  Devices.getDevice()
        debugger;
        if(res instanceof Array){
            res.forEach((device)=>{
                device.icon = 'icon-air-device'
                device.name = device.typeName2
                device.grade = 2
                device.subMedicalList.forEach((record)=>{
                    record.icon = 'icon-air-device'
                    record.name = device.medicalName
                    device.grade = 3
                })
            })
            return res
        }
        else{
            return []
        }
        debugger;
    }
    public async initTitleData(){
        return []
    }
    public async initTableData(){
        return []
    }
}

export default new DeviceData()
