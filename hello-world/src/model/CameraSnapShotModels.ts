import Vue from 'vue';
import { API, axios } from '../api';
import WebData from './WebData';

/**
 * 模型列表数据
 */
export default class CameraSnapShotModel {
  private webData = WebData.getInstance();
  private queryParams: object = {
    plan_id: this.webData.planID,
  };
  private removeParams: object = {
    id: 0,
    plan_id: this.webData.planID,
    auth: this.webData.auth,
  };
  private addParams: object = {
    /* class_type: 0,
    flashtype: 'tob',
    uid: 6425,*/
    type: 'normal',
    plan_id: this.webData.planID,
    auth: this.webData.auth,
    image: '',
    id: 0,
    camera: {
      angleX: 0,
      angleY: 0,
      position: { x: 0, y: 0, z: 0 },
      fov: 0,
      innerDepth: 1000000,
    },
  };

  constructor() {
    //  this.initParams();
  }

  /**
   * 当前的请求params
   */
  public get getQueryParams(): any {
    return this.queryParams;
  }
  public get getAddParams(): any {
    return this.addParams;
  }
  public get getRemoveParams(): any {
    return this.removeParams;
  }

  /**
   * 初始化所有请求参数
   */
  public initParams() {
    // 使用Vue赋值，来返回响应式的变量
    /*Vue.prototype.$set(this, 'queryParams', Object.assign({}, this.initParamsData));
    Vue.prototype.$set(this, 'addParams', Object.assign({}, this.initParamsData));
    Vue.prototype.$set(this, 'removeParams', Object.assign({}, this.initParamsData));*/
  }

  /**
   * 设置请求参数
   * @param key
   * @param val
   */
  public setQueryParams(key, val) {
    this.queryParams[key] = val;
  }
  public setAddParams(key, val) {
    this.addParams[key] = val;
  }
  public setRemoveParams(key, val) {
    this.removeParams[key] = val;
  }
  /*  public getObject(key, val) {
    const obj: any = {};
    obj[key] = val;
    return obj;
  }*/

  /**
   * 获取模型请求
   */
  public async get(params = {}) {
    params = { ...this.queryParams, ...params };
    const res = await axios.get(API.CAMERASNAPSHOT_URL, { params });

    return res;
  }

  public async add(params = {}) {
    params = { ...this.addParams, ...params };
    const res = await axios.put(API.CAMERASNAPSHOT_URL, params);

    return res;
  }
  public async remove(params = {}) {
    params = { ...this.removeParams, ...params };
    const res = await axios.delete(API.CAMERASNAPSHOT_URL, { params });

    return res;
  }
}
