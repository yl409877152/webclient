import Vue from 'vue';
import { API, axios } from '../api';

/**
 * 模型列表数据
 */
export default new class Devices {
  private deviceParams: object = {};
  private initDeviceParams: object = {
  };

  constructor() {
    this.initParams();
  }

  /**
   * 当前的请求params
   */
  public get queryInitOptions(): object {
    return this.initDeviceParams;
  }

  public get queryOptions(): object {
    return this.deviceParams;
  }

  /**
   * 初始化所有请求参数
   */
  public initParams() {
    // 使用Vue赋值，来返回响应式的变量
    Vue.prototype.$set(this, 'deviceParams', Object.assign({}, this.initDeviceParams));
  }

  /**
   * 设置请求参数
   * @param key
   * @param val
   */
  public setParams(key, val) {
    this.deviceParams[key] = val;
  }

  /**
   * 获取设备树请求
   */
  public async getDevice(params = {}) {debugger;
    params = { ...this.deviceParams, ...params };
console.log(API)
    const res = await axios.sget(API.DEVICE_HOST_URL, { params });

    return res;
  }
}
