import Vue from 'vue';
import LabelInput from './LabelInput';

export default function factory(options) {
  let LabelEditComponent = Vue.extend(LabelInput);
  let instance = new LabelEditComponent({
    propsData: options,
  });
  instance.vm = instance.$mount();

  return instance;
}
