import Vue from 'vue';
import LabelEdit from './LabelEdit';

export default function factory(options) {
  let LabelEditComponent = Vue.extend(LabelEdit);
  let instance = new LabelEditComponent({
    propsData: options,
  });
  instance.vm = instance.$mount();

  return instance;
}
