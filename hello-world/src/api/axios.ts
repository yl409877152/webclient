import _axios from 'axios';
import debounce from 'lodash/debounce';
import Vue from 'vue';
import WebData from '../model/WebData';
import { decryptByDESModeCBC } from './crypto';

const axios:any = _axios;

axios.defaults.timeout = 5000;
axios.defaults.headers.common.Accept = '*/*';

const addAuth = data => {
  let auth = '';
  //   auth = WebData.getInstance().auth

  return {
    ...data,
    // auth,
  };
};
const objectToUrlParams = param => {
  let paramStr = '';
  for (const i in param) {
    if (param.hasOwnProperty(i)) {
      paramStr = paramStr + i + '=' + param[i] + '&';
    }
  }
  return paramStr.substr(0, paramStr.length - 1);
};
// http request 向服务器请求拦截器
axios.interceptors.request.use(
  config => {
    if (config.method === 'get') {
      config.params = addAuth(config.params);
    } else if (config.method === 'post') {
      config.params = addAuth({
        // uid: WebData.getInstance().uid,
      });
    }
    return config;
  },
  error => {
    console.info('到request拦截器了error了');
    return Promise.reject(error);
  },
);

// http response 服务器回答拦截器
axios.interceptors.response.use(
  response => {debugger;
  console.log(response)
    if (response.status === 200) {
      return response.data;
    }
  },
  error => {debugger;
   updateMessage(error);
    console.info('到response拦截器了error了')
    return Promise.reject(error)
  },
);
const updateMessage = debounce(error => {
  let message = '网络错误';
  if (error.response) {
    if (error.response.status / 100 === 4) {
      message = '请求错误';
    }
    if (error.response.status / 100 === 5) {
      message = '服务器处理异常';
    }
  }
  Vue.prototype.$mess({
    type: 'error',
    message,
  });
}, 200);

export default class extends axios {
  public static CancelToken = axios.CancelToken;

  /**
   * 加密获取
   * @param {*} args
   */
  public static async sget(...args) {debugger;
    try {
      const res = await axios.get(...args);
      let data = res;
      // if (typeof res === 'string') {
      //   data = JSON.parse(decryptByDESModeCBC(res));
      // }
      return data;
    } catch (e) {

    }
  }

  public static async spost(...args) {
    try {
      const res = await axios.post(...args);
    //  const data = JSON.parse(decryptByDESModeCBC(res));
      return res;
    } catch (e) {
      console.warn('加密发送数据错误！');
    }
  }
  // public static async sendData(options) {
  //   console.log(options);
  //   const res = await axios(options);
  //   return res;
  // }
}

// export default axios
