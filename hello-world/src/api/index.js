import _axios from './axios';
import _API from './urls';

export let axios = _axios;
export let API = _API;
