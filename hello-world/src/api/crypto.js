/**
 * example:
 * import {encryptByDESModeCBC,decryptByDESModeCBC} from '../../api/crypto'
 * var cipherData = 'TUMAX'
 * //加密
 * var result = encryptByDESModeCBC(cipherData)
 * //解密
 * var result = decryptByDESModeCBC(cipherData)
 */

var CryptoJS = require('crypto-js');

const key = 'trg452a5';

// CBC模式加密
export function encryptByDESModeCBC(message) {
  var keyHex = CryptoJS.enc.Utf8.parse(key);
  var ivHex = CryptoJS.enc.Utf8.parse(key);
  // var data = CryptoJS.enc.Utf8.parse(JSON.stringify(message))
  var data = CryptoJS.enc.Utf8.parse(message);
  var encrypted = CryptoJS.DES.encrypt(data, keyHex, {
    iv: ivHex,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  var resultCipher = encrypted.ciphertext.toString(CryptoJS.enc.Base64);
  // console.info(resultCipher)
  return resultCipher;
}

// CBC模式解密
export function decryptByDESModeCBC(cipherData) {
  var myPattern = new RegExp('<.*>.*</.*>|<.*/>');
  // var myPattern = /<.*>.*<\/.*>|<.*\/>/
  cipherData = cipherData.replace(myPattern, '');

  var lenStr = cipherData.length;
  var startAt;
  var iStart, iEnd;
  for (iStart = 0; iStart < lenStr; iStart++) {
    startAt = cipherData.charCodeAt(iStart);
    if (startAt !== 0) {
      break;
    }
  }
  for (iEnd = lenStr - 1; iEnd >= 0; iEnd--) {
    startAt = cipherData.charCodeAt(iEnd);
    if (startAt !== 0) {
      break;
    }
  }
  cipherData = cipherData.slice(iStart, iEnd + 1);

  var keyHex = CryptoJS.enc.Utf8.parse(key);
  var ivHex = CryptoJS.enc.Utf8.parse(key);
  var decrypted = CryptoJS.DES.decrypt(
    {
      ciphertext: CryptoJS.enc.Base64.parse(cipherData),
    },
    keyHex,
    {
      iv: ivHex,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    },
  );

  var resultDecrypt = decrypted.toString(CryptoJS.enc.Utf8);
  // console.info(resultDecrypt)
  return resultDecrypt;
}
