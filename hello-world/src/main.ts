import store from "./store";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Message from "./components/Message";
import CollapseTransition from "element-ui/lib/transitions/collapse-transition";
import "element-ui/lib/theme-chalk/index.css";
import Echarts from 'echarts'
import {
    Button,
    Checkbox,
    CheckboxGroup,
    Collapse,
    CollapseItem,
    Dialog,
    Tooltip,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    Input,
    Slider,
    Popover,
    Form,
    FormItem,
    InputNumber,
    Radio,
    Autocomplete,
    Upload,
    TabPane,
    Tabs,
    Table,
    TableColumn,
    Row,
    Col,
    Menu,
    Submenu,
    MenuItem,
    MenuItemGroup,
    Tree,
} from "element-ui";
Vue.component(CollapseTransition.name, CollapseTransition);

import Movue from 'xb.movue';
import { reaction } from "mobx"
// @ts-ignore
Vue.use(Movue, { reaction });
Vue.use(Autocomplete);
Vue.use(Radio);
Vue.use(CheckboxGroup);
Vue.use(Col);
Vue.use(Button);
Vue.use(Dialog);
Vue.use(Popover);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Dropdown);
Vue.use(Checkbox);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Tooltip);
Vue.use(Input);
Vue.use(Slider);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(InputNumber);

Vue.use(Upload);
Vue.use(TabPane);
Vue.use(Tabs);

Vue.use(Table);
Vue.use(TableColumn);
Vue.use(Col);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
Vue.use(MenuItemGroup);
Vue.use(Tree);
// 处理消息提醒
Vue.prototype.$mess = Message;
Vue.config.productionTip = false;

Vue.prototype.$echarts = Echarts
router.push("/realValue"); // 刷新进入主页
new Vue({
    router,
    store,
    render: (h) => h(App),
    created() { },
    mounted() { },
}).$mount("#app") //
