/**
 * 用于await 方式处理异步等待
 * @param {*} time
 */

export function wait(time) {
  return new Promise(resolve => {
    setTimeout(resolve, time);
  });
}

export function download(url) {
  const aLink = document.createElement('a');
  const lastIndex = url.lastIndexOf('/') + 1;
  let fileName = url.substring(lastIndex);
  if (fileName.indexOf('?') > -1) {
    fileName = fileName.split('?')[0];
  }
  aLink.download = fileName;
  aLink.href = url;
  const evt = document.createEvent('MouseEvents');
  evt.initMouseEvent('click', false, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null); //initEvent 不加后两个参数在FF下会报错
  aLink.dispatchEvent(evt);
}

/**
 * 获取元素的相对位置
 * @param {*} obj
 */
export function getElementOffset(obj, parent = document.body) {
  var pos = {
    top: 0,
    left: 0,
  };
  if (obj.offsetParent !== parent && obj.offsetParent) {
    while (obj !== parent && obj.offsetParent) {
      pos.top += obj.offsetTop;
      pos.left += obj.offsetLeft;
      obj = obj.offsetParent;
    }
  } else if (obj.x) {
    pos.left += obj.x;
  } else if (obj.x) {
    pos.top += obj.y;
  }
  return {
    x: pos.left,
    y: pos.top,
  };
}

export function isiPhone() {
  return /(iPhone|iPad)/.test(window.navigator.userAgent);
}

/**
 * 动态加载js文件
 * @param _url 文件地址
 * @param _callBack 加载成功的回调
 * @param _propObj 需要给标签设置的属性值(例如：script.id = xxx script.name = xxx)
 */
export function loadScript(_url, _callBack, _propObj) {
  let script = document.createElement('script');
  script.type = 'text/javascript';
  if (typeof _propObj != 'undefined') {
    for (let key in _propObj) {
      script[key] = _propObj[key];
    }
  }
  script.src = _url;
  document.body.appendChild(script);
  if (typeof _callBack != 'undefined') {
    script.onload = () => {
      _callBack();
    };
  }
}

/**
 * 判断用户操作系统
 * @returns {string}
 */
export function getOs() {
  var OsObject = '';
  if (navigator.userAgent.indexOf('SE 2.X') > 0) {
    return 'sougou'; //sougou浏览器
  }
  if (navigator.userAgent.indexOf('MSIE') > 0) {
    return 'MSIE'; //ie浏览器
  }
  if (navigator.userAgent.indexOf('Firefox') > 0) {
    return 'Firefox'; //Firefox浏览器
  }
  if (navigator.userAgent.indexOf('Chrome') > 0) {
    return 'Chrome'; //Chrome浏览器
  }

  if (navigator.userAgent.indexOf('Safari') > 0) {
    return 'Safari'; //Safari浏览器
  }
  if (navigator.userAgent.indexOf('Gecko/') > 0) {
    return 'Gecko';
  }
}

/**
 * 通过event获取相对位置，兼容IOS
 * @param event
 * @returns {{x: number, y: y}}
 */
export function getOffsetByEvent(event) {
  let rect = event.target.getBoundingClientRect();
  return {
    x: event.pageX - rect.left,
    y: event.pageY - rect.top,
  };
}

/**
 * http(s) => https
 * @param url
 * @returns {*}
 */
export function translateToHttps(url) {
  let finalPath = url;
  if (finalPath.substring(0, 4) === 'http' && finalPath.substring(0, 5) !== 'https') {
    let protocol = 'https';
    let domain = finalPath.substring(4, finalPath.length - 1);
    finalPath = protocol.concat(domain);
    // finalPath = pathURL.concat('1');
  }

  return finalPath;
}

/**
 * 预览图URL处理
 * @param url
 * @param isPanorama 是否是全景图
 */
export function previewImgUrl(url, isPanorama) {
  return isPanorama ? url.replace(/(.*).jpg$/, '$1_f.jpg') : url;
}
