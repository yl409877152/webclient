import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/views/HelloWorld.vue'
import Default from '@/views/index.vue'
import RealValue from '@/views/realValue/index.vue'
import Tendency from '@/views/tendency/index.vue'
import Alarm from '@/views/alarm/index.vue'
import Main from '@/views/main.vue'
import Message from '@/components/Message';

Vue.use(Router)

const routeConf = new Router({
    base: '/mgms-core-web/',
    routes: [
        {
            path: '/gallery/',
            // name: 'gallery',
            //  component: Gallery,
            children: [],
        },
        {
            path: '/',
            name: 'Default',
            component: Default,
            meta: {
                keepAlive: true,
            },
            children: [
                {
                    path: '/',
                    name: 'main',
                    component: Main,
                    meta: {
                        keepAlive: true,
                    },
                    children: [
                        {
                            path: '/realValue',
                            name: 'realValue',
                            component: RealValue,
                        },
                        {
                            path: '/tendency',
                            name: 'tendency',
                            component: Tendency,
                        },
                        {
                            path: '/alarm',
                            name: 'alarm',
                            component: Alarm,
                        },
                    ],
                },

            ],
        },
    ],
})
let msgBox
routeConf.beforeResolve((to, from, next) => {
    msgBox = Message({
        message: '正在加载，请稍后...',
        showClose: false,
        type: 'info',
        duration: 0,
    })
    next()
})

routeConf.afterEach((to, from) => {
    !!msgBox && msgBox.close()
    msgBox = null
})
export default routeConf
