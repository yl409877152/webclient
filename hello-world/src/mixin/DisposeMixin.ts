/**
 * Vue组件 监听事件释放mixin
 * 用法：mixin到有需要监听事件释放的vue组件中，将需要释放的函数push到disposeArr中
 */
export default {
  data() {
    return {
      disposeArr: [],
    };
  },
  beforeDestroy() {
    for (const dispose of this.disposeArr) {
      dispose();
    }
  },
};
