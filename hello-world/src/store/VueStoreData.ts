import { action, computed, observable } from 'mobx'

class VueStoreData {
    @observable public designTabShow: string = 'stage1'
    @observable public _name: string = 'stage1'

    public setDesignTabShow(val: any) {
        this.designTabShow = val
    }
    @computed
    public get name() {
        return this._name
    }
    @action
    public fetchUsers() { }
}

export default new VueStoreData()
